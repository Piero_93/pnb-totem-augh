/** **************************************************************************************************************** **/
/**            User authorization                                                                                    **/
/** **************************************************************************************************************** **/
var Promise = require("bluebird");
var auth = require('basic-auth');

exports.checkUserAuthorization = function (req, trx) {
    var credentials = auth(req);
    if(credentials != null) {
        return exports.findUserWithPassword(credentials.name, credentials.pass, trx);
    } else {
        return new Promise(function (resolve) {
            resolve([]);
        });
    }
};

exports.findUser = function (userId, trx) {
    var query = global.knex;
    if (trx != null) {
        query = query.transacting(trx);
    }

    return query
        .select("*")
        .from("users")
        .where({
            userid: userId
        });
};

exports.findUserWithPassword = function (userId, password, trx) {
    var query = global.knex;
    if (trx != null) {
        query = query.transacting(trx);
    }

    return query
        .select("*")
        .from("users")
        .where({
            userid: userId,
            password: password
        });
};

/** **************************************************************************************************************** **/
/**            Provider authorization                                                                                **/
/** **************************************************************************************************************** **/
exports.checkProviderAuthorization = function (req, trx) {
    var credentials = auth(req);
    if(credentials != null) {
        return exports.findProviderWithPassword(credentials.name, credentials.pass, trx);
    } else {
        return new Promise(function (resolve) {
            resolve([]);
        });
    }
};

exports.findProvider = function (providerId, trx) {
    var query = global.knex;
    if (trx != null) {
        query = query.transacting(trx);
    }

    return query
        .select("*")
        .from("provider")
        .where({
            providerid: providerId
        });
};

exports.findProviderWithPassword = function (providerId, password, trx) {
    var query = global.knex;
    if (trx != null) {
        query = query.transacting(trx);
    }

    return query
        .select("*")
        .from("provider")
        .where({
            providerid: providerId,
            password: password
        });
};

/** **************************************************************************************************************** **/
/**            Totem authorization                                                                                   **/
/** **************************************************************************************************************** **/
exports.checkTotemAuthorization = function (req, trx) {
    var credentials = auth(req);
    if(credentials != null) {
        return exports.findTotemWithPassword(credentials.name, credentials.pass, trx);
    } else {
        return new Promise(function (resolve) {
            resolve([]);
        });
    }
};

exports.findTotemWithPassword = function (totemid, password, trx) {
    var query = global.knex;
    if (trx != null) {
        query = query.transacting(trx);
    }

    return query
        .select("*")
        .from("totem")
        .where({
            totemid: totemid,
            password: password
        });
};