'use strict';

//var url = require('url');

var positionManagerConstructor = require('./PositionManagement').PositionManager;
var PositionManager = new positionManagerConstructor('amqp://biaginirouter.myddns.me:5672', 10, 10000);

setTimeout(function () {
    global.knex("totem").select("*")
        .then(function (rows) {
            for(var i = 0; i < rows.length; i++) {
                var totem = rows[i];
                PositionManager.addTotem(totem.totemid, {
                    latitude: totem.latitude,
                    longitude: totem.longitude
                });
            }}).catch(function (error) {
        console.log("Couldn't load totem list! Error: " + JSON.stringify(error));
    });
}, 1000);

var userConstructor = require('./User').User;
var User = new userConstructor(PositionManager);

var totemConstructor = require('./Totem').Totem;
var Totem = new totemConstructor(PositionManager);

var providerConstructor = require('./Provider').Provider;
var Provider = new providerConstructor(PositionManager);

var administrationConstructor = require('./Administration').Administration;
var Administration = new administrationConstructor(PositionManager);

module.exports.administrationDDLPOST = function administrationDDLPOST (req, res, next) {
    Administration.administrationDDLPOST(req.swagger.params, res, next);
};

module.exports.contentContentIdGET = function contentContentIdGET (req, res, next) {
    Totem.contentContentIdGET(req.swagger.params, res, next);
};

module.exports.contentContentIdPOST = function contentContentIdPOST (req, res, next) {
    Provider.contentContentIdPOST(req.swagger.params, res, req, next);
};

module.exports.contentContentIdDELETE = function contentContentIdDELETE (req, res, next) {
    Provider.contentContentIdDELETE(req.swagger.params, res, req, next);
};

module.exports.indexTagGET = function indexTagGET (req, res, next) {
    Totem.indexTagGET(req.swagger.params, res, next);
};

module.exports.providerPOST = function providerPOST (req, res, next) {
    Provider.providerPOST(req.swagger.params, res, next);
};

module.exports.providerProviderIdContentsGET = function providerProviderIdContentsGET (req, res, next) {
    Provider.providerProviderIdContentsGET(req.swagger.params, res, req, next);
};

module.exports.providerProviderIdContentsPOST = function providerProviderIdContentsPOST (req, res, next) {
    Provider.providerProviderIdContentsPOST(req.swagger.params, res, req, next);
};

module.exports.totemPOST = function totemPOST (req, res, next) {
    Totem.totemPOST(req.swagger.params, res, next);
};

module.exports.userPOST = function userPOST (req, res, next) {
    User.userPOST(req.swagger.params, res, next);
};

module.exports.userUserIdInterestsGET = function userUserIdInterestsGET (req, res, next) {
    User.userUserIdInterestsGET(req.swagger.params, res, req, next);
};

module.exports.userUserIdInterestsPOST = function userUserIdInterestsPOST (req, res, next) {
    User.userUserIdInterestsPOST(req.swagger.params, res, next);
};

module.exports.userUserIdPositionPOST = function userUserIdPositionPOST (req, res, next) {
    User.userUserIdPositionPOST(req.swagger.params, res, req, next);
};

module.exports.userUserIdPositionGET = function userUserIdPositionGET (req, res, next) {
    User.userUserIdPositionGET(req.swagger.params, res, req, next);
};

module.exports.tagsGET = function tagsGET (req, res, next) {
    User.tagsGET(req.swagger.params, res, next);
};
