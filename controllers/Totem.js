'use strict';

exports.Totem = function (PositionManagement) {
    var Utilities = require('./Utilities');

    this.contentContentIdGET = function (args, res, next) {
        /**
         * parameters expected in the args:
         * contentId (String)
         **/
        var contentData;
        knex.transaction(function (trx) {
            return trx("content")
                .where({
                    contentid: args.contentId.value
                }).select("url", "expirationdate", "providerid", "contentid", "revision")
                .then(function (contentRow) {
                    if (contentRow.length < 1) {
                        res.status(404).end();
                        throw "Transaction correctly terminated (content not found)";
                    }

                    contentData = contentRow[0];
                    contentData.rev = contentData.revision;
                    contentData.id = contentData.contentid;
                    contentData.providerId = contentData.providerid;
                    contentData.expirationDate = contentData.expirationdate;

                    delete contentData["contentid"];
                    delete contentData["revision"];
                    delete contentData["providerid"];
                    delete contentData["expirationdate"];

                    return trx("category")
                        .where({contentid: args.contentId.value})
                        .select("tag");
                }).then(function (tagRows) {
                    contentData.tag = [];
                    for (var i = 0; i < tagRows.length; i++) {
                        contentData.tag.push(tagRows[i].tag);
                    }

                    res.json(contentData);
                });
        }).catch(function (error) {
            if (!res.headersSent) {
                res.status(500).json(error);
            }
        });
    };

    this.indexTagGET = function (args, res, next) {
        /**
         * parameters expected in the args:
         * tag (String)
         * limit (Integer)
         **/
        var tag = args.tag.value;
        var limit = args.limit.value;

        global.knex.select("*")
            .from("category")
            .where({
                tag: tag
            }).join("content", "content.contentid", "category.contentid")
            .limit(limit)
            .then(function (rows) {
                var contentIds = [];
                for(var i = 0; i < rows.length; i++) {
                    contentIds.push(rows[i].contentid);
                }

                return global.knex.select("*")
                    .from("category")
                    .whereIn('category.contentid', contentIds)
                    .join("content", "content.contentid", "category.contentid");
            })
            .then(function(contentsWithTag) {
                var result = [];
                for(var i = 0; i < contentsWithTag.length; i++) {
                    var obj = null;
                    for(var j = 0; j < result.length; j++) {
                        if(result[j].id == contentsWithTag[i].contentid) {
                            obj = result[j];
                            break;
                        }
                    }

                    if(obj == null) {
                        obj = {
                            id : contentsWithTag[i].contentid,
                            url : contentsWithTag[i].url,
                            expirationDate : contentsWithTag[i].expirationdate,
                            revision: contentsWithTag[i].revision,
                            tag : []
                        };

                        result.push(obj);
                    }

                    obj.tag.push(contentsWithTag[i].tag);
                }

                console.log("INDEX TAG: " + JSON.stringify(result));
                res.json(result);
            })
            /*.map(function (row) {
                return {
                    id: row.contentid,
                    tag: row.tag,
                    expirationDate: row.expirationdate,
                    url: row.url
                };
            })*//*.then(function (result) {
            res.json(result);
        })*/.catch(function (error) {
            console.log("Index tag error: " + JSON.stringify(error));
            if (!res.headersSent) {
                res.status(500).json(error);
            }
        });
    };

    this.totemPOST = function (args, res, next) {
        /**
         * parameters expected in the args:
         * credentials (TotemData)
         **/
        // no response value expected for this operation
        if (args.credentials != null && args.credentials.value != null) {
            var knex = global.knex;
            console.log(args.credentials.value);
            var totemId = args.credentials.value.totemId;
            var password = args.credentials.value.password;
            var latitude = args.credentials.value.position.latitude;
            var longitude = args.credentials.value.position.longitude;
            var visualizable_announces = args.credentials.value.visualizable_announces;

            var errors = {};
            var error = false;

            if (totemId == undefined || totemId.length <= 4) { //Check dello Username
                errors["tooShortUsername"] = true;
                error = true;
            }

            if (password == undefined || password.length <= 4) { //Check della Password
                errors["tooShortPassword"] = true;
                error = true;
            }

            if (errors.tooShortUsername != true) { //Se lo Username è accettabile, controllo che non esista già
                knex.transaction(function (trx) {
                    return trx("totem").select("totemid")
                        .where({
                            totemid: totemId
                        })
                        .then(function (rows) {
                            if (rows.length > 0) { //Utente già esistente
                                errors["duplicatedUsername"] = true;
                                error = true;
                            }

                            if (error == true) {
                                res.status(422).json(errors);
                                throw "Transaction closed (invalid registration data)";
                            }

                            return trx("totem")
                                .insert({
                                    totemid: totemId,
                                    password: password,
                                    latitude: latitude,
                                    longitude: longitude,
                                    nvisualizableannounces: visualizable_announces
                                });
                        }).then(function (dbResult) {
                            res.end();
                            PositionManagement.addTotem(totemId, args.credentials.value.position);
                    });
                }).catch(function (error) {
                    if (!res.headersSent) {
                        res.status(500).json(error);
                    }
                });
            } else {
                if (!res.headersSent) {
                    res.status(422).json(errors);
                }
            }
        } else {
            if (!res.headersSent) {
                res.status(422).end();
            }
        }
    };

};
