'use strict';

exports.User = function(PositionManagement) {
    var Utilities = require('./Utilities');
    var getAllTags = function (trx) {
        var query = global.knex;
        if (trx != null) {
            query = query.transacting(trx);
        }

        return query
            .select("tag")
            .from("tag")
            .map(function (row) {
                return row.tag;
            });
    };

    this.userPOST = function (args, res, next) {
        //INSERT INTO Users (UserId,Password,Latitude,Longitude) VALUES ('user','password',0.01, 0.01);
        /**
         * parameters expected in the args:
         * credentials (Credentials)
         **/
        if (args.credentials.value != undefined) {
            var username = args.credentials.value.userId;
            var password = args.credentials.value.password;

            var errors = {};
            var error = false;

            if (username == undefined || username.length <= 4) { //Check dello Username
                errors["tooShortUsername"] = true;
                error = true;
            }

            if (password == undefined || password.length <= 4) { //Ceck della Password
                errors["tooShortPassword"] = true;
                error = true;
            }

            if (errors.tooShortUsername != true) { //Se lo Username è accettabile, controllo che non esista già
                Utilities.findUser(username)
                    .then(function (rows) {
                        if (rows.length > 0) { //Utente già esistente
                            errors["duplicatedUsername"] = true;
                            error = true;
                        }
                        if (error == true) {
                            res.status(422).json(errors);
                        } else {
                            global.knex("users")
                                .insert({
                                    userid: username,
                                    password: password,
                                    latitude: 0.0,
                                    longitude: 0.0
                                })
                                .then(function (result) {
                                    res.end();
                                }).catch(function (error) {
                                res.status(422).json(error);
                            });
                        }
                    }).catch(function (pgError) {
                    res.status(500).json(pgError);
                });
            } else if (error == true) {
                res.status(422).json(errors);
            }
        } else {
            res.status(422).end();
        }
    };

    this.userUserIdInterestsGET = function (args, res, req, next) {
        var knex = global.knex;
        var userId = args.userId.value;
        var isUser = req.get('Entity') != "Totem";

        knex.transaction(function (trx) {
            var getInterests = function () {
                return Utilities.findUser(userId, trx)
                    .then(function(userRow) {
                        if(userRow.length > 0) {
                            return trx('interest').where(
                                {userid: userId}
                            ).select('tag')
                                .map(function (row) {
                                    return row.tag;
                                })
                                .then(function (tags) {
                                    res.json(tags);
                                });
                        } else {
                            res.status(404).end();
                        }
                    });
            };

            var thenable;
            if (isUser) {
                thenable = Utilities.checkUserAuthorization(req, trx);
            } else {
                thenable = Utilities.checkTotemAuthorization(req, trx);
            }

            return thenable
                .then(function (user) {
                    if (user.length > 0 && ((!isUser) || (userId == user[0].userid))) {
                        return getInterests();
                    } else {
                        res.status(401).end();
                        throw "End transaction";
                    }
                });
        }).catch(function (error) {
            if(!res.headersSent) {
                res.status(500).json(error);
            }
        });
    };

    this.userUserIdInterestsPOST = function (args, res, next) {
        var knex = global.knex;
        var userId = args.userId.value;
        var interests = args.interests.value;
        var rows = [];
        for (var i = 0; i < interests.length; i++) {
            rows.push({userid: userId, tag: interests[i]});
        }

        //console.log("Pushing interests for user " + userId + "as:");
        //console.log(rows);
        var badTags = false;

        knex.transaction(function (trx) {
            return Utilities.findUser(userId, trx)
                .then(function (user) {
                    if (user.length > 0) {
                        return knex('interest')
                            .transacting(trx)
                            .where({
                                userid: userId
                            }).del();
                    } else {
                        res.status(404).end();
                        throw "Terminate transaction";
                    }
                }).then(function (ignored) {
                    badTags = true;
                    return knex.batchInsert('interest', rows, 30).transacting(trx);
                }).then(function (success) {
                    badTags = false;
                    //console.log("Interests saved for user " + userId);
                    res.end();
                });
        }).catch(function (error) {
            //console.log(error);
            if (!res.headersSent) {
                var status = badTags ? 422 : 500;
                res.status(status).send();
            }
        });
    };

    this.userUserIdPositionPOST = function (args, res, req, next) {
        /**
         * parameters expected in the args:
         * userId (String)
         * position (Position)
         **/
        // no response value expected for this operation
        var userId = args.userId;
        if (userId != null && args.position != null) {
            userId = userId.value;
            knex.transaction(function (trx) {
                return Utilities.checkUserAuthorization(req, trx)
                    .then(function(user) {
                        if (user.length <= 0 || userId != user[0].userid) {
                            res.status(401).end();
                            throw "End transaction";
                        }

                        return trx("users")
                            .where("userid", args.userId.value)
                            .update({
                                latitude: args.position.value.latitude,
                                longitude: args.position.value.longitude
                            });
                    }).then(function (resp) {
                        res.end();
                        PositionManagement.notifyTotems(args.userId.value, args.position.value);
                    });
            }).catch(function (transactionError) {
                if (!res.headersSent) {
                    res.status(500).json(transactionError);
                }
            });
        } else {
            res.status(422).end();
        }
    };

    this.userUserIdPositionGET = function (args, res, req, next) {
        /**
         * parameters expected in the args:
         * userId (String)
         **/
        var userId = args.userId.value;
        var isUser = req.get('Entity') != "Totem";

        knex.transaction(function (trx) {
            var getPosition = function () {
                return Utilities.findUser(userId, trx)
                    .then(function(user) {
                        if(user.length > 0) {
                            res.json({latitude: user[0].latitude, longitude: user[0].longitude});
                        } else {
                            res.status(404).end();
                        }
                    });
            };

            var thenable;
            if (isUser) {
                thenable = Utilities.checkUserAuthorization(req, trx);
            } else {
                thenable = Utilities.checkTotemAuthorization(req, trx);
            }

            return thenable
                .then(function (user) {
                    if (user.length > 0 && ((!isUser) || (userId == user[0].userid))) {
                        return getPosition();
                    } else {
                        res.status(401).end();
                        throw "End transaction";
                    }
                });
        }).catch(function (error) {
            if(!res.headersSent) {
                res.status(500).json(error);
            }
        });
    };

    this.tagsGET = function tagsGET(req, res, next) {
        getAllTags()
            .then(function (tags) {
                res.json(tags);
            });
    };
};



