exports.PositionManager = function (url, distanceThreshold, retryTime) {
    var amqp = require('amqplib');
    var haversine = require('haversine');
    var queuePrefix = "totem-";
    if (retryTime == null || retryTime < 0) {
        retryTime = 10000;
    }

    if (distanceThreshold == null || distanceThreshold < 0) {
        distanceThreshold = 10;
    }

    var totemData = [];
    var totemConnections = [];
    var totemPendingConnections = [];
    var connection;
    var connecting = false;

    var tryTotemConnection = function (totemId) {
        console.log("TRY TOTEM " +  totemId);
        setTimeout(function () {
            var chan;
            if (!(totemId in totemConnections)) {
                if (!(totemId in totemPendingConnections)) {
                    if (connection != null) {
                        totemPendingConnections[totemId] = true;
                        connection.createChannel()
                            .then(function (channel) {
                                chan = channel;
                                channel.on('close', function () {
                                    delete totemConnections[totemId];
                                    setTimeout(function () {
                                        tryTotemConnection(totemId);
                                    }, retryTime);
                                });
                                channel.on('error', function () {
                                    delete totemConnections[totemId];
                                    setTimeout(function () {
                                        tryTotemConnection(totemId);
                                    }, retryTime);
                                });
                                console.log("Channel ok, Trying connection to totem queue " + queuePrefix + totemId);

                                return channel
                                    .assertQueue(queuePrefix + totemId, {
                                        durable: false,
                                        exclusive: false,
                                        autoDelete: false
                                    });
                            }).then(function (ok) {
                            delete totemPendingConnections[totemId];
                            totemConnections[totemId] = chan;
                            console.log("Queue asserted for " + queuePrefix + totemId);
                        }).catch(function (err) {
                            console.log("Can't connect to " + queuePrefix + totemId + " queue. Error = " + JSON.stringify(err));
                            setTimeout(function () {
                                tryTotemConnection(totemId);
                            }, retryTime);
                            delete totemPendingConnections[totemId];
                        });

                    } else {
                        console.log("No connection created, trying again later");
                        setTimeout(function () {
                            tryTotemConnection(totemId);
                        }, retryTime);
                    }
                }
            }
        }, 0);
    };

    var trySendNotify = function (totemId, userId, userPosition) {
        setTimeout(function () {
            var jsonStr = JSON.stringify({
                userId: userId,
                position: userPosition
            });
            if (totemId in totemConnections) {
                console.log("Sending message to queue " + queuePrefix + totemId);
                totemConnections[totemId].sendToQueue(queuePrefix + totemId, Buffer.from(jsonStr));
            }
        }, 0);
    };

    var createConnection = function () {
        if (!connecting) {
            connecting = true;

            if (connection != null) {
                connection.close();
            }

            amqp.connect(url).then(function (conn) {
                connection = conn;
                connecting = false;
                connection.on('close', function () {
                    setTimeout(createConnection, retryTime);
                });
                connection.on('error', function () {
                    setTimeout(createConnection, retryTime);
                });
            }).catch(function (err) {
                connecting = false;
                console.log("Can't connect to AMPQ server. Error = " + JSON.stringify(err));
                setTimeout(createConnection, retryTime);
            });
        }
    };

    this.notifyTotems = function (userId, position) {
        var userPosition = {
            latitude: position.latitude,
            longitude: position.longitude
        };

        console.log("Notifying : " + userId + " with position " + JSON.stringify(userPosition));

        for (var totemId in totemData) {
            var distance = haversine(totemData[totemId], userPosition, {unit: 'meter'});
            if (distance < distanceThreshold) {
                console.log("Totem " + totemId + " found at distance " + distance);
                trySendNotify(totemId, userId, userPosition);
            }
        }

        /*var queueNae = queuePrefix + "";

         // Publisher
         open.then(function(conn) {
         return conn.createChannel();
         }).then(function(ch) {
         return ch.assertQueue(q).then(function(ok) {
         return ch.sendToQueue(q, new Buffer('something to do'));
         });
         }).catch(console.warn);*/
    };

    this.addTotem = function (totemId, position) {
        console.log("Adding totem " + totemId + " with position " + JSON.stringify(position));
        totemData[totemId] = {
            latitude: position.latitude,
            longitude: position.longitude
        };

        tryTotemConnection(totemId);

        /*var queueNae = queuePrefix + "";

         // Publisher
         open.then(function(conn) {
         return conn.createChannel();
         }).then(function(ch) {
         return ch.assertQueue(q).then(function(ok) {
         return ch.sendToQueue(q, new Buffer('something to do'));
         });
         }).catch(console.warn);*/
    };

    createConnection();
};

