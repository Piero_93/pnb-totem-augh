'use strict';

exports.Administration = function (PositionManagement) {
    this.administrationDDLPOST = function (args, res, next) {
        //console.log(args);
        if (args.ddl != null && args.ddl.value.length > 0) {
            global.knex
                .raw("DROP SCHEMA public CASCADE")
                .then(function (resp) {
                    return global.knex.raw("CREATE SCHEMA public");
                })
                .then(function (resp) {
                    //Create db
                    var lines = args.ddl.value.split(/[\n\r]/g);
                    var withoutEmptyLines = [];
                    for (var i = 0; i < lines.length; i++) {
                        if (lines[i].length > 0 &&
                            (!lines[i].startsWith("--")) &&
                            (!lines[i].toLowerCase().startsWith("create database"))) {
                            withoutEmptyLines.push(lines[i]);
                        }
                    }

                    var queries = withoutEmptyLines.join("").split(";");
                    console.log(queries);

                    var executeNextQuery = function (index) {
                        if (!(index < queries.length) ) {
                            res.end();
                            return;
                        } else if (queries[index].length > 0) {
                            queries[index] = queries[index].trim();
                            console.log("Executing: " + queries[index]);
                            return global.knex.raw(queries[index])
                                .then(function (result) {
                                    return executeNextQuery(index + 1);
                                })
                                .catch(function (error) {
                                    res.status(422).send(error);
                                });
                        } else {
                            return executeNextQuery(index + 1);
                        }
                    };

                    executeNextQuery(0);
                });
        } else {
            res.status(422).end();
        }
    };
};
