'use strict';

exports.Provider = function (PositionManagement) {
    var Utilities = require('./Utilities');
    this.providerPOST = function (args, res, next) {
        /**
         * parameters expected in the args:
         * credentials (Credentials)
         **/
        // no response value expected for this operation
        if (args.credentials.value != undefined) {
            var username = args.credentials.value.userId;
            var password = args.credentials.value.password;

            var errors = {};
            var error = false;

            if (username == undefined || username.length <= 4) { //Check dello Username
                errors["tooShortUsername"] = true;
                error = true;
            }

            if (password == undefined || password.length <= 4) { //Check della Password
                errors["tooShortPassword"] = true;
                error = true;
            }

            if (errors.tooShortUsername != true) { //Se lo Username è accettabile, controllo che non esista già
                global.knex().select("providerid")
                    .from("provider")
                    .where({
                        providerid: username
                    })
                    .then(function (rows) {
                        if (rows.length > 0) { //Utente già esistente
                            errors["duplicatedUsername"] = true;
                            error = true;
                        }
                        if (error == true) {
                            res.status(422).json(errors);
                        } else {
                            global.knex("provider")
                                .insert({
                                    providerid: username,
                                    password: password
                                })
                                .then(function (result) {
                                    res.end();
                                }).catch(function (error) {
                                res.status(422).json(error);
                            });
                        }
                    }).catch(function (pgError) {
                    res.status(500).json(pgError);
                });
            } else if (error == true) {
                res.status(422).json(errors);
            }
        } else {
            res.status(422).end();
        }
    };

    this.providerProviderIdContentsGET = function (args, res, req, next) {
        /**
         * parameters expected in the args:
         * providerId (String)
         **/
        var providerId = args.providerId.value;

        knex.transaction(function (trx) {
            return Utilities.checkProviderAuthorization(req, trx)
                .then(function (provider) {
                    if (provider.length <= 0 || providerId != provider[0].providerid) {
                        res.status(401).end();
                        return;
                    }

                    return trx('content')
                        .where({
                            providerid: providerId
                        }).select('contentid')
                        .map(function (row) {
                            return row.contentid;
                        })
                        .then(function (contentIds) {
                            res.json(contentIds);
                        });
                });
        }).catch(function (transactionError) {
            res.status(500).json(transactionError);
        });
    };

    this.providerProviderIdContentsPOST = function (args, res, req, next) {
        /**
         * parameters expected in the args:
         * providerId (String)
         * content (ContentRegistrationData)
         **/
        var providerId = args.providerId.value;
        var tags = args.content.value.tag;
        var expirationDate = args.content.value.expirationDate;
        var url = args.content.value.url;

        var contentId;
        var knex = global.knex;
        knex.transaction(function (trx) {
            return Utilities.checkProviderAuthorization(req, trx)
                .then(function (provider) {
                    if (provider.length <= 0 || providerId != provider[0].providerid) {
                        res.status(401).end();
                        throw "End transaction";
                    }

                    return trx("content")
                        .insert({
                            expirationdate: expirationDate,
                            providerid: providerId,
                            url: url,
                            revision: 0
                        }, 'contentid');
                }).then(function (insertedId) {
                    contentId = insertedId[0];

                    var rows = [];
                    for (var i = 0; i < tags.length; i++) {
                        rows.push({
                            contentid: contentId,
                            tag: tags[i]
                        });
                    }

                    return trx('category').insert(rows);
                }).then(function (result) {
                    res.end(contentId + "");
                })
        }).catch(function (transactionError) {
            if(!res.headersSent) {
                res.status(500).json(transactionError);
            }
        });
    };

    this.contentContentIdPOST = function(args, res, req, next) {
        var contentId = args.contentId.value;
        var contentData = args.content.value;

        var knex = global.knex;
        var providerId;
        knex.transaction(function (trx) {
            return Utilities.checkProviderAuthorization(req, trx)
                .then(function (provider) {
                    if (provider.length <= 0) {
                        res.status(401).end();
                        throw "End transaction (credentials error)";
                    }

                    providerId = provider[0].providerid;

                    return trx("content")
                        .select("providerid", "revision")
                        .where({
                            contentid: contentId
                        });
                }).then(function (existingContentRow) {
                    if(existingContentRow.length <= 0) {
                        res.status(404).end();
                        throw "End transaction (content not found)";
                    }

                    if(existingContentRow[0].providerid != providerId) {
                        console.log("Invalid credentials: " + existingContentRow[0].providerid + " vs " + providerId);
                        res.status(401).end();
                        throw "Creator id doesn't match requestor id";
                    }

                    return trx('content')
                        .where({
                            contentid: contentId
                        }).update({
                            url: contentData.url,
                            expirationdate: contentData.expirationDate,
                            revision: parseInt(existingContentRow[0].revision) + 1
                        });
                }).then(function (result) {
                    return trx('category')
                        .where({
                            contentid: contentId
                        }).delete();
                }).then(function (result) {
                    var rows = [];
                    for (var i = 0; i < contentData.tag.length; i++) {
                        rows.push({
                            contentid: contentId,
                            tag: contentData.tag[i]
                        });
                    }

                    return trx('category').insert(rows);
                }).then(function(result) {
                    res.end();
                });
        }).catch(function (transactionError) {
            if(!res.headersSent) {
                res.status(500).json(transactionError);
            }
        });
    };

    this.contentContentIdDELETE = function (args, res, req, next) {
        var contentId = args.contentId.value;

        var knex = global.knex;
        var providerId;
        knex.transaction(function (trx) {
            return Utilities.checkProviderAuthorization(req, trx)
                .then(function (provider) {
                    if (provider.length <= 0) {
                        res.status(401).end();
                        throw "End transaction (credentials error)";
                    }

                    providerId = provider[0].providerid;

                    return trx("content")
                        .select("providerid")
                        .where({
                            contentid: contentId
                        });
                }).then(function (existingContentRow) {
                    if(existingContentRow.length <= 0) {
                        res.status(404).end();
                        throw "End transaction (content not found)";
                    }

                    if(existingContentRow[0].providerid != providerId) {
                        res.status(401).end();
                        throw "Creator id doesn't match requestor id";
                    }

                    return trx('category')
                        .where({
                            contentid: contentId
                        }).delete();
                }).then(function (result) {
                    return trx('content')
                        .where({
                            contentid: contentId
                        }).delete();
                }).then(function(result) {
                    res.end();
                });
        }).catch(function (transactionError) {
            if(!res.headersSent) {
                res.status(500).json(transactionError);
            }
        });
    };
};

